#
# Be sure to run `pod lib lint BaseKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BaseKit'
  s.version          = '4.1.5'
  s.summary          = '基础库kit.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
这是一个基础类库.
                       DESC

  s.homepage         = 'https://bitbucket.org/haiyongou/BaseKit'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'haiyongou' => 'haiyongou@163.com' }
  s.source           = { :git => 'https://bitbucket.org/haiyongou/BaseKit.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  # s.source_files = 'BaseKit/Classes/**/*'
  s.source_files = 'BaseKit/Classes/*','BaseKit/Classes/**/*'
  s.requires_arc = true # 是否启用ARC
  
  non_arc_files = 'BaseKit/Classes/vendor/JSONKit/JSONKit.{h,m}' # 这是需要添加mrc标识的文件，为相对路径
  s.exclude_files = non_arc_files # 在工程中首先排除一下
  s.subspec 'no-arc' do |sp| # 子设置，为需要添加mrc标识的文件进行设置
      sp.source_files = non_arc_files
      sp.requires_arc = false
  end
  
  # s.resource_bundles = {
  #   'BaseKit' => ['BaseKit/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  
  s.frameworks   = "UIKit", "Foundation","Security","CFNetwork","CoreGraphics","CoreText" #支持的框架
  s.libraries = "iconv", "xml2","stdc++","stdc++.6.0.9","c++","icucore","z","sqlite3"
  
  s.dependency 'AFNetworking', '~> 3.0'    #网络请求
  s.dependency 'FontIcon', '~> 1.0'  #字体库, 包含601 个图标,大部分是 IOS7 style
  s.dependency 'MGJRouter', '~> 0.10.0' #url路由
  s.dependency 'MBProgressHUD', '~> 0.9' #进度条提示
  s.dependency 'FMDB', '~> 2.7.2' #sqlite操作库
  s.dependency 'SDWebImage','~> 3.7', '!= 3.7.2'   #显示网络图片 由于MWPhotoBrowser组件限制了，暂时未使用最新版
  s.dependency 'JSONModel', '~> 1.7.0'  #json构建model
  s.dependency 'TTTAttributedLabel', '~> 2.0.0'  #富文本label
  # s.dependency 'JSONKit', '~> 1.5pre' #json转换类
  s.dependency 'Masonry', '~> 1.1.0' #自动布局
  s.dependency 'UICKeyChainStore', '~> 2.1.1' #本地生产keyChain
  s.dependency 'GCDObjC', '~> 0.3.0' #GCDObjC封装了GCD
  
end
